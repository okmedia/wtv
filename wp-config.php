<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy, używanej lokalizacji WordPressa
 * i ABSPATH. Więćej informacji znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'walbrzych24_0007');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'walbrzych24_0007');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'wtv24walbrzych');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NB.}{XMi.pca+l_YnEp 1*(k~[<(5,|AgLfw+U_ti)63#g.7ByPgLFE8[^8l&=/B');
define('SECURE_AUTH_KEY',  ';ycLIzib?nlkT38a!uu`?VNG@UhPZ4GJK dJ*=8JK(()XYu9`0(H%<})|x[+qWZE');
define('LOGGED_IN_KEY',    'Bga/D@;i9Bs6k?R2|BgQOV,U.c2]&0O{S08;g4B& p-u$Q!;)UXz JM/eX4t#0P<');
define('NONCE_KEY',        'S7N]0u|0D>Aej FOYi;^%xM8|n{$X-b<O+ ^7r p3)%{2C#YsAL+1~nV+:vdu|D~');
define('AUTH_SALT',        't{bcjt{T|Dkuq,`Q4D80n@h%,Igle#-_NmrIW<[:4w#~ncQ;|kc`S*^3jb:yY*S9');
define('SECURE_AUTH_SALT', 'u`t-P;dbjT$[|r-c=O1#kY]J/VmO5oMQG#2K0g&[h]cT|q5|/-r(ccD_k9;8*&-4');
define('LOGGED_IN_SALT',   'm%hu0T%>`;J`|`/K|-R_w.bxnxvk$w+<YA(ZEZbM)Mtpoi#EK;QR|ipplqZ#k8U7');
define('NONCE_SALT',       'tMA@/l&0!v1Kp<4{cN)H+69_115I+WaDIa@@gx&90V+Ww_A_UtT,||C@J-l=j.+B');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Kod lokalizacji WordPressa, domyślnie: angielska.
 *
 * Zmień to ustawienie, aby włączyć tłumaczenie WordPressa.
 * Odpowiedni plik MO z tłumaczeniem na wybrany język musi
 * zostać zainstalowany do katalogu wp-content/languages.
 * Na przykład: zainstaluj plik de_DE.mo do katalogu
 * wp-content/languages i ustaw WPLANG na 'de_DE', aby aktywować
 * obsługę języka niemieckiego.
 */
define('WPLANG', 'pl_PL');

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
